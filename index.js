console.log("Hello");

let getCube = Math.pow(2, 3);

let address = [`258 Washington Ave`, `NW`, `California`, `90011`];

let [street, direction, state, zip] = address;

let animal = {
	name: `Lolong`,
	species: `saltwater crocodile`,
	weight: `1075 kgs`,
	length: `20 ft 3 in`
}

let {name, species, weight, length} = animal;

class Dog {
	constructor (name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

let myDog = new Dog(`Frankie`, 5, `Miniature Dachshund`);


console.log(`The cube of 2 is ${getCube}`);

console.log(`I live at ${street} ${direction},
${state} ${zip}`);

console.log(`${name} was a ${species}.
He wieghed at ${weight} with a measurement of ${length}.`);

console.log(myDog);